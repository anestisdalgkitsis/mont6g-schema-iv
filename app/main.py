# MONT-6G SERVICE LATENCY BENCHMARK v1.19.19
# Anestis Dalgkitsis | Started 13/10/2023

# Dependencies
import re
import time
import uuid
import yaml
import pickle
import redis
import subprocess

# Functions

def downloadContainerJSON():
	hashes = redis_client.hgetall("functions")
	function_list = []
	for key, value in hashes.items():
		key = key.decode('utf-8')
		value = pickle.loads(value)
		function_list.append(value)
	return function_list

def ping_ip(ip_address):
	process = subprocess.Popen(["ping", "-c", "1", ip_address], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	process.wait()
	output, error = process.communicate()
	output = output.decode('utf-8')
	time_match = re.search(r'time=([\d.]+) ms', output)
	if time_match:
		ping_time = float(time_match.group(1))
		return ping_time
	else:
		return None

# INITIALIZATION
print("MONT-6G SERVICE LATENCY BENCHMARK ENDPOINT")
print("Initializing...")

# load configuration file
print("Loading configuration file")
with open('config.yaml','r') as file:
	config = yaml.safe_load(file)
redis_host = config['redis_host']
redis_port = config['redis_port']
redis_password = config['redis_password']
print("Configuration file loaded successfully")

# Create a Redis client
redis_client = redis.Redis(host=redis_host, port=redis_port, password=redis_password)

# Test the connection
try:
	redis_client.ping()
	print('Successfully connected to Redis.')
except redis.ConnectionError:
	print('Failed to connect to Redis.')

# SHELL
while True:
	target_container_uid = input("Target Container UID: ")
	containerData = downloadContainerJSON()
	if all(item["uid"] == target_container_uid for item in containerData):
		print("Container is locked. Initiating benchmark... Press ctrl+c to exit.")
		break
	else:
		print("Target Container does not exist. Try pasting the uid from the Shell, nodes list. ") 

# LOOP
while True:
	containerData = downloadContainerJSON()

	# Get IP and Sample
	for container in containerData:
		if container.get("uid") == target_container_uid:
			host_ip = container.get("hosted_ip")
			latency_sample = ping_ip(host_ip)
			print("Latency Sample: " + str(latency_sample) + " ms")
		else:
			print("CONTAINER OFFLINE. Skipping")

	# Report
	redis_client.set("latency", latency_sample)
	print("Sample Reported.")
	time.sleep(1)

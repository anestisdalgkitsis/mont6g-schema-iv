# PROJECT MONT-6G REMOTE INTERFACE SHELL v2.3.15
# Anestis Dalgkitsis | Started 26/07/2023

# Dependencies
import time
import uuid
import yaml
import pickle
import pprint
import redis

# FUNCTIONS
def help_message():
	print("Commands: start, stop, nodes, containers, status, iteration, help, exit.")
	print("Debug Commands: clearnodes, clearcontainers.")

def start_procedure():
	user_input = input("Confirm System Startup (in 10s)? (y/n) >>] ")
	user_input_clean = user_input.lower().replace(" ", "")
	if user_input_clean == "y":
		current_timestamp = time.time()
		start_timestamp = int(current_timestamp) + 10
		print("Updating DB...")
		redis_client.set("start_time", start_timestamp)
		redis_client.set("system_status", "SCHEDULED")
		redis_client.set("iteration", 0)
		redis_client.set("function_index", 0)
		time.sleep(0.1)
		if str(redis_client.get("start_time").decode('utf-8')) == str(start_timestamp):
			#print("confirmed!")
			#print("Start time set! Start time: " + str(start_timestamp))
			print("Confirmed! Start time in 10s at: " + str(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_timestamp))))
		else:
			print("start time could not be set! Check db connection")
		return
	elif user_input_clean == "n":
		return
	else:
		print("invalid input. Returning.")
	return

def stop_procedure():
	redis_client.set("system_status", "STANDBY")
	redis_client.set("start_time", "NULL")
	time.sleep(0.1)
	redis_client.publish("schema-bus", "stop")
	if str(redis_client.get("system_status").decode('utf-8')) == str("STANDBY"):
		print("STOP confirmed by DB.")
	else:
		print("System could not be stopped, could not connect to db. Manual shutdown is required.")
	return

def list_agents():
	print("Registered Nodes:")
	hashes = redis_client.hgetall("nodes")
	for key, value in hashes.items():
		key = key.decode('utf-8')
		value = pickle.loads(value)
		pprint.pprint(value)

def clear_nodes():
	hashes = redis_client.hgetall("nodes")
	for key, value in hashes.items():
		key = key.decode('utf-8')
		redis_client.hdel("nodes", key)
	print("Done.")

def clear_containers():
	hashes = redis_client.hgetall("functions")
	for key, value in hashes.items():
		key = key.decode('utf-8')
		redis_client.hdel("functions", key)
	print("Done.")

def list_functions():
	print("Registered Containers:")
	hashes = redis_client.hgetall("functions")
	for key, value in hashes.items():
		key = key.decode('utf-8')
		value = pickle.loads(value)
		pprint.pprint(value)

def check_status():
	print(str(redis_client.get("system_status").decode('utf-8')))

def check_iteration():
	print(str(redis_client.get("iteration").decode('utf-8')))

# INITIALIZATION

# Welcome Message
print("MONT-6G BUSINESS INTERFACE SHELL")
print("Initializing...")

# load configuration file
print("Loading configuration file")
with open('config.yaml','r') as file:
        config = yaml.safe_load(file)
redis_host = config['redis_host']
redis_port = config['redis_port']
redis_password = config['redis_password']
print("Configuration file loaded successfully")

# Create a Redis client
redis_client = redis.Redis(host=redis_host, port=redis_port, password=redis_password)

# Test the connection
try:
    redis_client.ping()
    print('Successfully connected to Redis.')
except redis.ConnectionError:
    print('Failed to connect to Redis.')

# Create a pubsub connection
#pubsub = redis_client.pubsub()

# SHELL

print("\nMONT-6G BUSINESS INTERFACE SHELL \nType \"help\" and press return for a command list")

# shell input loop
while True:
	user_input = input(">>] ")
	user_input_clean = user_input.lower().replace(" ", "")

	if user_input_clean == "start":
		start_procedure()
	elif user_input_clean == "stop":
		stop_procedure()
	elif user_input_clean == "nodes":
		list_agents()
	elif user_input_clean == "clearnodes":
		clear_nodes()
	elif user_input_clean == "clearcontainers":
		clear_containers()
	elif user_input_clean == "containers":
		list_functions()
	elif user_input_clean == "status":
		check_status()
	elif user_input_clean == "iteration":
		check_iteration()
	elif user_input_clean == "help":
		help_message()
	elif user_input_clean == "exit" or user_input_clean == "quit" or user_input_clean == "q":
		break
	elif user_input_clean == "":
		pass
	else:
		print("Unknown command \"" + user_input + "\". Type help and press return for more information.")

# Close the Redis connection
redis_client.close()
print("DB connection closed. Exiting...")
exit()

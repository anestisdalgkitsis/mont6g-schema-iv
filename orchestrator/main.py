# MONT-6G ORCHESTRATOR
# Anestis Dalgkitsis | Started 23/07/2023

# Import Dependencies
import os
import signal
import sys
import time
import uuid
import yaml
import pickle
import redis
import psutil
import docker
import socket
import paramiko
import threading
from tqdm import tqdm
import netifaces as ni
#from kubernetes import client, config
import random

# Import Local
from rlagent import RLAgent
print("\n")

# Global Variables
AGENT_VERSION = "v2.2.15"
AGENT_UUID = str(uuid.uuid4())
AGENT_NAME = socket.gethostname()
AGENT_IP = ni.ifaddresses('enp5s0')[ni.AF_INET][0]['addr']
AGENT_STATUS = "INITIALIZING"
AGENT_OPINION = ""
AGENT_HEARTBEAT = int(time.time())
AGENT_ITERATION = 0
AGENT_FUNCTION_INDEX = 0

SYSTEM_TIMEOUT = 120 # Seconds

# Threading flags
stop_flag = threading.Event()

# ctrl+c signal handling
def exit_signal(signal, frame):
	print("\nExit Procedure, DO NOT INTERRUPT!")
	redis_client.hdel("nodes",AGENT_UUID)
	print("Done. Exit!")
	sys.exit(0)

# Register ctrl+c signal handler
signal.signal(signal.SIGINT, exit_signal)

# FUNCTIONS

def handle_message(message):
	message = message["data"].decode('utf-8')
	print("Message received: " + message)
	if message == "stop":
		print("STOP requested from shell.")
		sys.exit(0)

def subscribe_to_channel():
	pubsub = redis_client.pubsub()
	pubsub.subscribe('schema-bus')
	for message in pubsub.listen():
		if message['type'] == 'message':
			handle_message(message)

def add_container(uuid_input, name_input, image_input, hostid_input, hostip_input, cpu_input, ram_input, status_input):
	cont_dict = {'uid': uuid_input, 'name': name_input, 'image': image_input, 'hosted_uid': hostid_input, 'hosted_ip': hostip_input, 'cpu': cpu_input, 'ram': ram_input, 'status': status_input}
	cont_pickle = pickle.dumps(cont_dict)
	try:
		redis_client.hset("functions", uuid_input, cont_pickle)
		print("saved_container to db OK: " + str(cont_dict))
	except Exception as e:
		print("ERROR SAVING CONTAINER TO DB" + str(cont_dict) + " exception: " + str(e))

def remove_container(cuid):
	hashes = redis_client.hgetall("functions")
	function_data_cache = []
	function_uuid_cache = []
	for key, value in hashes.items():
		key = key.decode('utf-8')
		value = pickle.loads(value)
		function_data_cache.append(value)
		function_uuid_cache.append(value['uid'])

	for i in range(0, len(function_data_cache)):
		if cuid not in function_data_cache[i]["uid"]:
			print("CUID not found in db container list! Aborting!")
			exit()
		else:
			try:
				redis_client.hdel("functions", function_uuid_cache[i])
				print("removed_container from DB OK: " + str(function_data_cache[i]["uid"]))
			except Exception as e:
				print("ERROR REMOVING CONTAINER FROM DB" + str(function_data_cache[i]["uid"]) + " exception: " + str(e))

def update_node_containers():
	# Get Redis Functions Snapshot
	hashes = redis_client.hgetall("functions")
	function_data_cache = []
	function_uuid_cache = []
	for key, value in hashes.items():
		key = key.decode('utf-8')
		value = pickle.loads(value)
		print (f'{key}: {value}')
		function_data_cache.append(value)
		function_uuid_cache.append(value['uid'])
	print(function_data_cache)
	print(function_uuid_cache)

	# Get and update Functions in Redis
	docker_client = docker.from_env()
	running_containers = docker_client.containers.list()
	print("cont in cont:")
	for container in running_containers:
		print("|-- " + str(container.id))
		if container.id not in function_uuid_cache:
			print("CC " + str(container.image.tags[0]))
			print("CC " + str(container.stats(stream=False)['cpu_stats']['cpu_usage']['total_usage']))
			print("CC " + str(container.stats(stream=False)['memory_stats']['usage']))
			add_container(container.id, container.name, container.image.tags[0], AGENT_UUID, AGENT_IP, container.stats(stream=False)['cpu_stats']['cpu_usage']['total_usage'], container.stats(stream=False)['memory_stats']['usage'], container.status)
		else:
			print("CC " + str(container.id) + " already registered, ckecking IP")

def update_node():
	node_dict = {'uid': AGENT_UUID, 'name': AGENT_NAME, 'ip': AGENT_IP, 'opinion': AGENT_OPINION, 'status': AGENT_STATUS, 'heartbeat': int(time.time())}
	node_pickle = pickle.dumps(node_dict)
	redis_client.hset("nodes", AGENT_UUID, node_pickle)

def block_until_synchronized(list, status):
	global AGENT_STATUS
	print("CHECKPOINT REACHED --> " + status)
	AGENT_STATUS = status
	update_node()
	start_timeout_time = int(time.time())
	retries = 0
	progress_bar = tqdm(total=SYSTEM_TIMEOUT, desc="Timeout threshold")
	while True:
		status_board = []
		hashes = redis_client.hgetall("nodes")
		for key, value in hashes.items():
			key = key.decode('utf-8')
			value = pickle.loads(value)
			status_board.append(value["status"])
		result = all(string == status for string in status_board)
		if result == True:
			progress_bar.close()
			print("All " + str(len(status_board)) +" agents time-synced in checkpoint with status: " + status)
			break

		current_time = int(time.time())
		if current_time - start_timeout_time >= SYSTEM_TIMEOUT:
			progress_bar.close()
			print("!!! TIMEOUT. EXITING, DO NOT INTERRUPT!")
			redis_client.hdel("nodes", AGENT_UUID)
			print("Done. Exit!")
			sys.exit(0)
		else:
			progress_bar.update(int(current_time - start_timeout_time) - progress_bar.n)
		time.sleep(0.1)
		retries += 1
	print("CHECKPOINT COMPLETE. SYNC DONE")
	return

# RL Related functions (to be moved in a gym file)

def get_obs():

	# Download function data
	hashes = redis_client.hgetall("functions")
	function_data_cache = []
	print("Listing functions data downloaded:")
	for key, value in hashes.items():
		key = key.decode('utf-8')
		value = pickle.loads(value)
		print (f'{key}: {value}')
		function_data_cache.append(value)
	print(function_data_cache)

	# Select Current VNF
	AGENT_FUNCTION_INDEX = int(redis_client.get("function_index").decode('utf-8'))
	print(AGENT_FUNCTION_INDEX)

	# Prepare the RL Agent observation
	observation = []

	# Fetch Function Data
	print("li->" + str(AGENT_FUNCTION_INDEX))
	print("lo->" + str(function_data_cache))
	if not function_data_cache:
		print("At least one container must be hosted in the network to activate the orchestrator. Exiting.")
		exit(-1)
	observation.append(int(function_data_cache[AGENT_FUNCTION_INDEX]["cpu"]))
	observation.append(int(function_data_cache[AGENT_FUNCTION_INDEX]["ram"]))
	#observation.append(int(function_data_cache[AGENT_FUNCTION_INDEX]["str_r"]))

	# Fetch Node Data
	observation.append(psutil.cpu_percent()) #interval=1
	observation.append(psutil.virtual_memory().percent)
	observation.append(psutil.disk_usage('/').percent)

	print("get_obs() result: ")
	print(observation)

	return observation

def get_sample_action():
	return 5

def gen_reward():
	reward_function = -(float(redis_client.get("latency")) * 1000) # testing only
	return reward_function

# INITIALIZATION

# Welcome Message
print("PROJECT MONT ORCHESTRATOR " + AGENT_VERSION)
print("\n")
print("Initializing...")

# load configuration file
print("Loading configuration file")
with open('config.yaml','r') as file:
	config2 = yaml.safe_load(file)
redis_host = config2['redis_host']
redis_port = config2['redis_port']
redis_password = config2['redis_password']
print("configuration file loaded successfully")

# Create a Redis client
redis_client = redis.Redis(host=redis_host, port=redis_port, password=redis_password)

# Test the connection
try:
	redis_client.ping()
	print('Successfully connected to Redis.')
except redis.ConnectionError:
	print('Failed to connect to Redis.')
	exit()

# Subscribe to messaging bus
subscription_thread = threading.Thread(target=subscribe_to_channel)
#subscription_thread.start()

# Connect to the local Docker Container
docker_client = docker.from_env()
print("Docker client: " +str(docker_client))

# Connect to the local Kubernetes Cluster
'''
try:
	config.load_kube_config()
	print("Successfully loaded kubeconfig from a specific location.")
	k8s_client = client.CoreV1Api()
	print("Listing pods with their IPs:")
	v1 = client.CoreV1Api()
	ret = v1.list_pod_for_all_namespaces(watch=False)
	for i in ret.items:
		print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))
	#setup here!
except Exception as e:
	print(f"K8s Setup error: {e}")
	time.sleep(10)
'''
update_node_containers()

# Initialize RL Agent
LocalAgent = RLAgent(get_obs(), get_sample_action())
print("---")
print("DQN Subsystem Initialized")

# WAITING TO START
print("Entering waiting mode. Waiting for start trigger from Shell")
AGENT_STATUS = "WAITING"
update_node()

while True:
	current_time = int(time.time())
	start_timestamp = redis_client.get("start_time").decode('utf-8')
	if str(redis_client.get("system_status").decode('utf-8')) == "SCHEDULED":
		if current_time >= int(start_timestamp):
			print("Flag down, GO!")
			break
		elif start_timestamp == "NULL":
			print("NULL waiting")
		else:
			print("                           ", end="\r", flush=True)
			print("SCHEDULED, countdown: " + str(int(start_timestamp) - current_time), end="\r", flush=True)
	elif str(redis_client.get("system_status").decode('utf-8')) == "ONLINE":
		print("Flag down, GO!")
		break
	else:
		print("NOT SCHEDULED. Waiting...", end="\r", flush=True)
	time.sleep(0.1)
AGENT_STATUS = "RUNNING"
update_node()
redis_client.set("system_status", "ONLINE")

# THE HYPERLOOP
print("Entering the hyperloop.")

while True:

	# [!] SYNC POINT A
	block_until_synchronized("nodes", "WAITING_CP-A")

	time.sleep(2)

	# CP-A (Voting)
	AGENT_STATUS = "CP-A_VOTING"
	update_node()
	update_node_containers()

	# [PRIOR_OBS] Retrieve an observation
	prior_observation = get_obs()

	# [ACTION] Think of an action
	action = LocalAgent.action(prior_observation)
	print("Agent bid is: " + str(action))
	AGENT_OPINION = action
	AGENT_STATUS = "CP-A_VOTED"
	update_node()
	update_node_containers()

	# Prepare System for the next VNF
	hashes = redis_client.hgetall("functions")
	if int(redis_client.get("function_index").decode('utf-8')) == AGENT_FUNCTION_INDEX:
		if AGENT_FUNCTION_INDEX < len(hashes)-1:
			AGENT_FUNCTION_INDEX += 1
		else:
			AGENT_FUNCTION_INDEX = 0
		redis_client.set("function_index", AGENT_FUNCTION_INDEX)
	else:
		AGENT_FUNCTION_INDEX = int(redis_client.get("function_index").decode('utf-8'))
	print("AGENT_FUNCTION_INDEX: " + str(AGENT_FUNCTION_INDEX))
	time.sleep (2)

	# [!] SYNC POINT B
	block_until_synchronized("nodes", "WAITING_CP-B")

	time.sleep(2)

	# CP-B (Orchestration)
	AGENT_STATUS = "CP-B_ORCHESTRATION"
	update_node()

	votes_board = []
	id_board = []
	value_board = []
	print("downloading the votes of other nodes...")
	hashes = redis_client.hgetall("nodes")
	for key, value in hashes.items():
		key = key.decode('utf-8')
		value = pickle.loads(value)
		if value["status"] != "blocked":
			votes_board.append(value["opinion"])
			id_board.append(key)
			value_board.append(value)
	print("nodes UUIDs and votes results:")
	print(id_board)
	print(votes_board)

	# Vote result
	winner_vote = max(votes_board)
	winner_node = votes_board.index(winner_vote)
	print("Winner UUID: " + str(id_board[winner_node]) + " Vote: " + str(winner_vote))
	#time.sleep(2)

	# Decision
	docker_client = docker.from_env()
	all_containers = docker_client.containers.list(all=True)
	container_list = []
	for container in all_containers:
		container_list.append(container.name)
	hashes = redis_client.hgetall("functions")
	function_list2 = []
	for key, value in hashes.items():
		key = key.decode('utf-8')
		value = pickle.loads(value)
		function_list2.append(value)
	iamthesource = False
	if function_list2[AGENT_FUNCTION_INDEX]["name"] in [container.name for container in all_containers]:
		print("This node ISSOURCE")
		iamthesource = True
	else:
		print("This node NOT SOURCE")
		iamthesource = False
	iamthedestination = False
	if AGENT_UUID == id_board[winner_node]:
		iamthedestination = True
	if iamthedestination and iamthesource:
		print("BOTH SOURCE AND DESTINATION, no action")
	elif iamthedestination:
		# if i am the destination then WAIT TO RECEIVE, UNPACK AND START
		print("WINNER - I AM THE DESTINATION")
		print("Waiting the container")
		start_time = time.time()
		while time.time() - start_time < SYSTEM_TIMEOUT:
			if os.path.exists(os.path.expanduser("~/schema-dep/orchestrator/inbox/transfer_image.tar")):
				print("FILE FOUND! CONTINUE")
				print("F Exists? " + str(os.path.exists(os.path.expanduser("~/schema-dep/orchestrator/inbox/transfer_image.tar"))))
				print("---")
				break
			else:
				print("---")
				print("File not found in the INBOX. Waiting for file...")
				print(str(os.listdir(os.path.expanduser("~/schema-dep/orchestrator/inbox/"))))
				print("---")
			time.sleep(1)
		print("FOUND! Time guard 2s...")
		time.sleep(2)
		# Unpack file
		try:
			tarpath = os.path.expanduser("~/schema-dep/orchestrator/inbox/transfer_image.tar")
			with open(tarpath, "rb") as tarball_file:
				docker_client.images.load(fileobj=tarball_file)
			print("Container loaded OK!")
		except docker.errors.APIError as e:
			print(f"Error loading image: {e}")
		except FileNotFoundError as e:
			print(f"File not found: {e}")
		except Exception as e:
			print(f"An unexpected error occurred while unpacking: {e}")
		# Run container
		#docker_client.containers.run(image='my-transfer-image:latest', detach=True)
		container = docker_client.containers.run(image=str(function_list2[AGENT_FUNCTION_INDEX]["image"]), detach=True)
		time.sleep(5)
		add_container(container.id, container.name, container.image.tags[0], AGENT_UUID, AGENT_IP, container.stats(stream=False)['cpu_stats']['cpu_usage']['total_usage'], container.stats(stream=False)['memory_stats']['usage'], container.status)
		# delete the file
		try:
			os.remove(os.path.expanduser("~/schema-dep/orchestrator/inbox/transfer_image.tar"))
			print("Transfer file removed successfully.")
		except OSError as e:
			print("Error removing transfer file: " + str(e))
	elif iamthesource:
		print("I AM THE SOURCE")
		# Select container
		container = docker_client.containers.get(str(function_list2[AGENT_FUNCTION_INDEX]["uid"]))
		# Export container state
		image = docker_client.images.get(str(function_list2[AGENT_FUNCTION_INDEX]["image"]))
		print("docker image get: " + str(image))

		try:
			output_path = "./outbox/transfer_image.tar"
			with open(output_path, "wb") as f:
				for chunk in image.save(named=True):
					f.write(chunk)
			print(f"Image saved to '{output_path}' successfully.")
		except docker.errors.APIError as e:
			print(f"Error saving image: {e}")
		except FileNotFoundError as e:
			print(f"Error: Output directory not found or inaccessible - {e}")
		except Exception as e:
			print(f"An unexpected error occurred while exporting: {e}")

		#print("Safe time sleep 10s")
		#time.sleep(10)

		# Stop the container, with timeout check
		print("Stopping/Killing container...")
		try:
			#container.stop()
			container.kill()
			print("waiting 2 s...")
			time.sleep(2)
			print("done. moving on.")
		except Exception as e:
			print("An unexpected error occurred while stopping:", e)
			exit()
		print("cont_id for removal: " + str(container.id))
		remove_container(container.id)

		# Transfer image
		print("|-- CREATING LOCAL SSH CLIENT")
		ssh_client = paramiko.SSHClient()
		ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		print("|-- CONNECTING TO REMOTE HOST...")
		try:
			ssh_client.connect(value_board[winner_node]['ip'], username='oki', password='fA5x')
			print("|-- CONNECTING VIA SFTP...")
			sftp = ssh_client.open_sftp()
			print("|-- UPLOADING...")
			sftp.put("./outbox/transfer_image.tar", "/home/oki/schema-dep/orchestrator/inbox/transfer_image.tar")
			print("|-- TERMINATING CONNECTION")
			sftp.close()
			ssh_client.close()
			print("|-- CONTAINER TRANSFER COMPLETE!")
			time.sleep(2)
			print("|-- CLEANING OUTBOX")
			os.remove(os.path.expanduser("./outbox/transfer_image.tar"))
		except Exception as e:
			print("An error occurred while removing transfer file: " + str(e))
			ssh_client.close()
			exit(-1)
		finally:
				# Close the SSH connection
				ssh_client.close()
		# IF COMPLETE WITH SUCCESS, DELETE THE IMAGE FROM THE OUTBOX
	else:
		# if i am not source or destination then DO NOTHING
		print("NO SRC OR DST, no action")

	# Global Update
	update_node_containers()

	time.sleep(2)

	# [!] SYNC POINT C
	block_until_synchronized("nodes", "WAITING_CP-C")

	time.sleep(2)

	# CP-C (Results)
	AGENT_STATUS = "CP-C_RESULTS"
	update_node()
	update_node_containers()

	# [POSTERIOR_OBS] Retrieve a new observation to generate rewards
	posterior_observation = get_obs()

	# [REWARD] Generate the reward
	reward = gen_reward()

	# [DONE]
	done = False # used for episode changing

	# [AGENT_LEARN]
	print("agent_learn")
	LocalAgent.learn(prior_observation, action, reward, posterior_observation, done)

	# [AGENT_REPLAY_MEMORY]
	print("agent_replay_memory")
	LocalAgent.replay()

	# [TARGET_TRAIN]
	print("agent_target_train")
	LocalAgent.train()

	# Stop the agent if requested by the Shell
	if str(redis_client.get("system_status").decode('utf-8')) == "STANDBY":
			print("\nExit Procedure requested by the Shell, DO NOT INTERRUPT!")
			redis_client.hdel("nodes", AGENT_UUID)
			print("Done. Exit!")
			sys.exit(0)

	# Prepare the system for the next iteration
	update_node_containers()
	if int(redis_client.get("iteration").decode('utf-8')) == AGENT_ITERATION:
		AGENT_ITERATION += 1
		redis_client.set("iteration", AGENT_ITERATION)
	else:
		AGENT_ITERATION = int(redis_client.get("iteration").decode('utf-8'))
	print("SYSTEM_ITERATION: " + redis_client.get("iteration").decode('utf-8') + " | AGENT_ITERATION: " + str(AGENT_ITERATION))

# Close the Redis connection
redis_client.close()

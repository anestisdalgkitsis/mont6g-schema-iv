# SCHEMA DQN RL AGENT
# Anestis Dalgkitsis | Based on the 7 December 2020 SCHEMA project

# Dependencies

import random
import numpy as np
from collections import deque

# Tensorflow Dependencies

import tensorflow as tf
from tensorflow import keras

# Keras API Dependencies

from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam

# CLASS

class RLAgent:
	def __init__(self, observation, action):

		# Environment
		self.observation_space = observation
		self.action_space = action

		# Memory Queue
		self.memory = deque(maxlen=2000)

		# Memory Replay Parameters
		self.batch_size = 32

		# Agent Hyperparameters
		self.gamma = 0.85
		self.epsilon = 1.0
		self.epsilon_min = 0.01
		self.epsilon_decay = 0.995
		self.learning_rate = 0.005
		self.tau = .125

		# Models
		self.model = self.compileAgentModel()
		self.target_model = self.compileAgentModel()

	def compileAgentModel(self):
		model = Sequential()
		model.add(Dense(24, input_dim=1, activation="relu")) # .shape[0]
		model.add(Dense(48, activation="relu"))
		model.add(Dense(24, activation="relu"))
		model.add(Dense(self.action_space)) # Default is linear activation # self.action_space.n # .shape[0]
		model.compile(loss="mean_squared_error", optimizer=Adam(lr=self.learning_rate)) # mse
		return model

	def action(self, state):
		self.epsilon *= self.epsilon_decay
		self.epsilon = max(self.epsilon_min, self.epsilon)
		if np.random.random() < self.epsilon:
			sample = np.random.random_sample(size = self.action_space)
			#return np.argmax(sample), max(sample)
			return np.argmax(sample)
		#return np.argmax(self.model.predict(state)[0]), max(self.model.predict(state)[0])
		return np.argmax(self.model.predict(state)[0])
	def learn(self, state, action, reward, new_state, done): #remember
		self.memory.append([state, action, reward, new_state, done])

	def replay(self):

		# Skip Memory Replay if Memory Batch is not full
		if len(self.memory) < self.batch_size:
			return

		# Replay Random Action from Memory Batch
		samples = random.sample(self.memory, self.batch_size)
		for sample in samples:
			state, action, reward, new_state, done = sample
			target = self.target_model.predict(state)
			if done:
				target[0][action] = reward
			else:
				Q_future = max(self.target_model.predict(new_state)[0])
				target[0][action] = reward + Q_future * self.gamma
			self.model.fit(np.asarray(state), target, epochs=1, verbose=0)

	def train(self):

		# Update Neural Network Weights
		weights = self.model.get_weights()
		target_weights = self.target_model.get_weights()
		for i in range(len(target_weights)):
			target_weights[i] = weights[i] * self.tau + target_weights[i] * (1 - self.tau)
		self.target_model.set_weights(target_weights)

	def test(self):
		pass

	def save(self, filename):
		self.model.save(filename)

	def load(self, filename):
		self.model = keras.models.load_model(filename)
